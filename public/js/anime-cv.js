$( document ).ready(function() {

	let level = $(".v");

	jQuery.each(level, function(){  

		let bar = $(this).parent("div");
		let valeur = $(this).val();

		switch(valeur){
			case '1':
			    bar.addClass("animate-1");
			    break;
			case '2':
			    bar.addClass("animate-2");
			    break;
			case '3':
			    bar.addClass("animate-3");
			    break;
			case '4':
			    bar.addClass("animate-4");
			    break;
		}
	});
})
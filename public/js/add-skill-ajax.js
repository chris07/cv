$(document).ready(function() {

    $('select').select2({
    	tags: true
    });

    $('select').on("change", function(e){

    	let name = $(this).find("[data-select2-tag=true]");
        	
    	if(name.length && $.inArray(name.val(), $(this).val()) !== -1 ){
    		$.ajax({
    			url:"/admin/skill/create/ajax/"+name.val(),
    			type:"POST"
    		}).done(function(data){
    			name.replaceWith(`<option selected value="${data.id}">${name.val()}</option>`);
    		});	
    	}
    	else{

   			name = null;
   		}	
    });
});
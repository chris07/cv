$( document ).ready(function() {
    
    $("input[type=file]").change(function () {

        var files = $(this).prop("files");
        var names = $.map(files, function(val) { return val.name; });
        var txt = "";

        if (files != undefined || files != "") {
        	
        	jQuery.each(names, function(i,v){ txt += v+" "; });
        }
        $(this).next(".custom-file-label").text(txt);
    });
});
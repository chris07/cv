-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Lun 28 Décembre 2020 à 16:23
-- Version du serveur :  5.7.32-0ubuntu0.18.04.1
-- Version de PHP :  7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `cv`
--

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20200716175002', '2020-07-16 19:50:10', 3402),
('DoctrineMigrations\\Version20200717090350', '2020-07-17 11:04:00', 838),
('DoctrineMigrations\\Version20200719210857', '2020-07-19 23:09:24', 2422),
('DoctrineMigrations\\Version20200719211249', '2020-07-19 23:12:57', 147),
('DoctrineMigrations\\Version20200801224601', '2020-08-02 00:46:12', 2294),
('DoctrineMigrations\\Version20200802003345', '2020-08-02 02:33:51', 1351),
('DoctrineMigrations\\Version20200809195625', '2020-08-09 21:56:36', 700),
('DoctrineMigrations\\Version20200809201155', '2020-08-09 22:12:01', 2357),
('DoctrineMigrations\\Version20200824120834', '2020-08-24 14:09:15', 3032),
('DoctrineMigrations\\Version20200824135542', '2020-08-24 15:55:50', 1616);

-- --------------------------------------------------------

--
-- Structure de la table `picture`
--

CREATE TABLE `picture` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `picture`
--

INSERT INTO `picture` (`id`, `project_id`, `name`) VALUES
(1, NULL, 'b6f2a864276a1f75e9d874472f35cbe4.png'),
(3, NULL, '20c6c4da11dfe61907a623d5d59f55a2.png'),
(4, NULL, '83b0f3cff4913ce776ee450426a599ba.png'),
(5, NULL, 'c9ac46edfbfc2b5dbd3fe565ea25497f.png'),
(6, NULL, '227bed10c4ee1f43833b4f250e428c40.png'),
(7, NULL, '3e000608a81079f79c6cd12a1716e70a.png'),
(8, NULL, 'cf836d180efd75819d457d38f0400c20.png'),
(9, NULL, '0f8f0867c911094ad1da4b0e31fe1462.png'),
(10, NULL, '78e6b5b3bfc50c75a1fe7f3d5696d057.png'),
(15, NULL, '7793445082b4c4c9c265ea6b3ecd7157.png'),
(16, NULL, 'fe3d692eed1cebbb13c5ab7979183d37.png'),
(17, NULL, 'c263ddf8126d28aa58f53380f41d8df3.png'),
(20, NULL, 'skyzoologo_blanc-e1589929285993.png'),
(22, NULL, 'd2d3c66d32598d26248035596daee827.png'),
(24, NULL, 'e889ab4ca0762c546b7d3b3805d00126.png'),
(25, NULL, 'e9a3d76b3fd2c9d45d89ba10f09ebcbd.png'),
(26, NULL, '56666eebfaaff909cc2d1148b0519d6b.png'),
(41, NULL, '204b1ca5cf7366762b5786b444d5adfd.png'),
(42, NULL, '6caf4f2220e3be121ff071b41ace21d0.png'),
(45, NULL, '93b88c43979c3f61f308a78f93b954a3.webp'),
(46, NULL, '0c706c945934d200fecad3d30573152b.webp'),
(47, NULL, '51ae8c2f5b108591a642985c391fea3d.webp'),
(48, NULL, '8b2ecb058ef58de0c46a40d44b04f42e.webp'),
(49, NULL, 'aa10bc21ae9e77c1c6f3c8a9e0abb426.webp'),
(50, NULL, '9c7bd3e7dcc83720a8ccd9edf70dfeec.webp'),
(51, NULL, 'ef51d44250a84de06a2fc6acdccb6a68.webp'),
(52, NULL, '9c6547de8d3fd4c44446cc5eccf979b1.webp'),
(53, NULL, 'e4fce06208ea4e340d1c19b28ae73e57.webp'),
(54, NULL, '4c5ca4ad2cf6b052f0a25ec8ce45382d.webp'),
(55, NULL, '5551f3a30f2ba89fc4905cbd8711f35a.webp'),
(56, NULL, '7687098765dd04db8d10a5095d85d988.webp'),
(57, NULL, '5232062c6165864b25f5a356e889ba01.webp'),
(58, NULL, 'f9a37be307537c22a48dc4560553ce0c.webp'),
(59, NULL, 'b3c3e0ab02cb649282c3c18db1d531cc.webp'),
(60, NULL, '4b063c6f3282342c351349d7b4b81c71.webp'),
(61, NULL, 'e41dfda2f73a1e6c0b598993599ae418.webp'),
(62, NULL, '0600d366aab3ce0588a02292561919dc.webp'),
(66, 10, '1878456fc866121b5e3e26f7a5f43c58.webp'),
(67, 10, '54f9c6c652b1de89db674b0e7c67fae3.webp'),
(68, 10, '905b4bae1d3f15efc9f7f57ff2b398bf.webp'),
(69, 11, 'b2260690e24ef7efaed2d1f7d4154ada.webp'),
(70, 11, '9b98e5c49c911b3d5822ac783419c39a.webp'),
(71, 11, 'af25a1348db11928e05f9fb654f928f2.webp'),
(72, 15, '9337e316d6d84eaa73de490797b17e4e.webp'),
(73, 15, '29b47c09296ac2f828d6168da8d042bc.webp'),
(74, 15, 'ab011dd006a7b89d5d822852ad764b1e.webp'),
(75, 13, 'f03e9444693788a6376cc975ecc1c460.webp'),
(76, 13, '30cd38f8ea76cef49c9adf7455886872.webp'),
(77, 13, 'c49f59b7840862d6f6150710991caa25.webp');

-- --------------------------------------------------------

--
-- Structure de la table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `project`
--

INSERT INTO `project` (`id`, `title`, `slug`, `summary`, `content`, `link`) VALUES
(1, 'FabLab Portal', 'fablab-portal', 'petit logiciel à destination d\'une association. Le but principal était pour eux d\'améliorer et de simplifier la gestion et la facturation pour l\'utilisation de leurs machines.', '<p style=\"text-align:center\"><strong>Mon premier projet!</strong></p>\r\n\r\n<p style=\"text-align:center\">Le projet que j&#39;ai du r&eacute;aliser lors de ma formation.</p>\r\n\r\n<p style=\"text-align:center\">C&#39;&eacute;tait un logiciel interne &agrave; l&#39;association pour la gestion du FabLab, il &eacute;tait fait entierrement en php &agrave; la papa,trop d&eacute;geu, &agrave; la rache avec un peu de recul. Mais bon, d&#39;apr&egrave;s les retours je suis all&eacute; bien plus loin qu&#39;ils pensaient au d&eacute;part et ils se sont servi de mon programme comme base pour en faire un nouveau plus fonctionnel.</p>\r\n\r\n<p style=\"text-align:center\">Et de mon cot&eacute;, cela m&#39;a fait une bonne base php pour commencer d&#39;autres projets!</p>\r\n\r\n<p style=\"text-align:center\">Reste que il n&#39;existe plus rien de ce code aujourd&#39;hui, sauf peut-&ecirc;tre quelques r&eacute;sidus oubli&eacute;s dans un micro-framework disparu.</p>', NULL),
(2, 'SymplyOrga', 'symplyorga', 'Petit logiciel pour une association, Ils voulaient surtout pouvoir suivre le paiement des cotisations des adhérents ainsi qu\'un système de budgétisation et d\'analyses de leurs dépences.', '<p style=\"text-align:center\"><strong>Logiciel php fait main!</strong></p>\r\n\r\n<p style=\"text-align:center\">Ce petit logiciel fait pour optimiser la gestion d&#39;une asso m&#39;a surtout servi &agrave; concevoir une sorte de mini framework php.</p>\r\n\r\n<p style=\"text-align:center\">A quoi cela sert de r&eacute;-inventer la roue ? me direz-vous.</p>\r\n\r\n<p style=\"text-align:center\">Et je r&eacute;pondrai que cela sert &agrave; mieux comprendre d&#39;o&ugrave; l&#39;on vient afin de mieux choisir vers quoi l&#39;on va.</p>\r\n\r\n<p style=\"text-align:center\">Le retour de l&#39;asso est pl&ucirc;tot bon , le fait de n&#39;utiliser aucun framework garantit de bonnes performances et visiblement ils ont d&eacute;cid&eacute; de le garder pour leur gestion. A ma connaissance ils l&#39;utilisent toujours.</p>\r\n\r\n<p style=\"text-align:center\">Pour ma part Ce projet m&#39;a fait comprendre en profondeur certains aspect de la conception d&#39;une application client-serveur, tout en comprenant la n&eacute;cessit&eacute; des frameworks actuels, et leur impact sur le temps pass&eacute; sur la maintenance du code.</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>', 'https://gitlab.com/chris07/simplyorga'),
(10, 'L\'entraide locale', 'l-entraide-locale', 'Site vitrine de l\'entreprise d\'aides à domicile. La demande était un site pas chère, facilement administrable et faisable rapidement.', '<p style=\"text-align:center\"><strong>Un petit site vitrine !</strong></p>\r\n\r\n<p style=\"text-align:center\">Comme dit, petit site vitrine pour une entreprise d&#39;aides &agrave; domicile. La patronne voulait pouvoir facilement d&eacute;l&eacute;guer la maintenance du site ou pouvoir la faire elle-m&ecirc;me. Du coup l&#39;emploi d&#39;un CMS comme Wordpress me semblait tout indiqu&eacute;.</p>\r\n\r\n<p style=\"text-align:center\">Au premier abord le retour fut bon, mais avec le temps je me rends compte des difficult&eacute;s pour des personnes non issues de la tech de maintenir leurs sites dans le long terme surtout avec Wordpress.</p>\r\n\r\n<p style=\"text-align:center\">Mais bon Wordpress m&#39;as quand m&ecirc;me permis un grand gain de temps de d&eacute;veloppement!</p>', 'https://lentraidelocale.com/'),
(11, 'Drome ardèche paysages', 'drome-ardeche-paysages', 'Site vitrine d\'un concepteur paysagiste.', '<p style=\"text-align:center\"><strong>Un autre site vitrine.</strong></p>\r\n\r\n<p style=\"text-align:center\">Pas trop de diff&eacute;rences avec les autres projets Wordpress.</p>\r\n\r\n<p style=\"text-align:center\">M&ecirc;mes contraintes de budget et de temps donc m&ecirc;mes technos.Le plus gros du travail pour ce genre de site &eacute;tant sur le SEO, je dois avouer qu&#39;un CMS est quand m&ecirc;me un s&eacute;rieux gain de temps.</p>\r\n\r\n<p style=\"text-align:center\">Je crois que l&#39;entreprise n&#39;existe cependant malheureusement plus.</p>', 'https://drome-ardeche-paysage.fr/'),
(13, 'Poplume', 'poplume', 'Site e-commerce de la savonnerie artisanale Poplume.', '<p style=\"text-align:center\"><strong>Site e-commerce de savons artisanaux.</strong></p>\r\n\r\n<p style=\"text-align:center\">Les demandes &eacute;tant encore sensiblement les m&ecirc;mes que les autres commer&ccedil;ants, j&#39;ai encore utilis&eacute; Wordpress pour ce projet.</p>\r\n\r\n<p style=\"text-align:center\">Je me suis surtout port&eacute; sur ce choix par rapport &agrave; la facturations, pas de ligne direct avec la banque et la volont&eacute; de passer par Stripe.</p>\r\n\r\n<p style=\"text-align:center\">Ceci-dit cela m&#39;int&eacute;resserait vraiment de voir comment g&eacute;rer manuellement les syst&egrave;mes de paiement par internet. Peut-&ecirc;tre dans un futur projet!</p>\r\n\r\n<p style=\"text-align:center\">De bons retours sur ce projet, et la commer&ccedil;ante g&egrave;re &ccedil;a toute seule ce qui montre qu&#39;avec un peu de formation, on peut rendre les commer&ccedil;ants quasi autonomes avec un bon CMS.</p>', 'https://poplume.com/'),
(14, 'Games Reviews', 'games-reviews', 'Site communautaire de notations et de revues de jeux vidéos par les utilisateurs.', '<p style=\"text-align:center\"><strong>Site communautaire de gamers.</strong></p>\r\n\r\n<p style=\"text-align:center\">Site dans le genre de&nbsp; <a href=\"https://www.gamespot.com/\">gamespot.com</a>&nbsp; mais en fran&ccedil;ais . Une sorte de petit r&eacute;seau social de gamers.</p>\r\n\r\n<p style=\"text-align:center\">Nous &eacute;tions plusieurs sur ce projet et les technos &eacute;taient donc impos&eacute;es. Cela m&#39;a permis d&#39;apprendre Laravel, que je trouve un peu trop restrictif dans le choix de ses composants mais bon une exp&eacute;rience tr&egrave;s int&eacute;ressante, pleins de fonctionnalit&eacute;s &agrave; mettre en place comme la messagerie, le syst&egrave;me de notation ...</p>\r\n\r\n<p style=\"text-align:center\">Nous avons d&eacute;cid&eacute; de fermer le site 1 an apr&egrave;s sa mise en prod &agrave; cause du manque de fr&eacute;quentation.&nbsp;</p>', NULL),
(15, 'Mon CV', 'mon-cv', 'Mon CV Numérique.', '<p style=\"text-align:center\"><strong>Mon Cv Num&eacute;rique.</strong></p>\r\n\r\n<p style=\"text-align:center\">Je voulais quelque chose de fiable, de performant, de maintenable et d&#39;&eacute;volutif.</p>\r\n\r\n<p style=\"text-align:center\">J&#39;ai donc choisi d&#39;utiliser Symfony pour ce projet. Je dois dire avoir longtemps h&eacute;sit&eacute; entre php et nodejs pour le back-end. Un site &agrave; tr&egrave;s peu de traffic comme celui l&agrave; aurait eu de meilleures performances avec nodejs, mais au final c&#39;est le support de HTTP2 par php qui a fini par me faire choisir php. Il sera bien plus facilement maintenable comme &ccedil;a je pense.</p>\r\n\r\n<p style=\"text-align:center\">Sinon projet int&eacute;ressant, Symfony me correspond plus dans son approche que Laravel et risque d&#39;orienter mes choix pour les futurs projets.</p>\r\n\r\n<p style=\"text-align:center\">Cot&eacute; front, c&#39;est du classique j&#39;avais pas envie de perdre en performances avec l&#39;emploi d&#39;un framework js, j&#39;avais juste besoin d&#39;un peu d&#39;asynchrone sur ce projet donc un peu de js suffit.</p>\r\n\r\n<p style=\"text-align:center\">Le point n&eacute;gatif de ce projet vient de l&#39;emploi de Bootstrap qui je l&#39;esp&eacute;rais me ferait gagner du temps, mais il a surtout ralentit les perf surtout &agrave; cause de sa d&eacute;pendance &agrave; JQuery. Cela devrait changer dans le futur mais mauvaise premi&egrave;re impression pour Bootstrap pour moi.</p>', 'https://gitlab.com/chris07/cv'),
(16, 'benevoles', 'benevoles', 'Projet Top Secret pour l\'instant. Tout ce que je peux dire c\'est que c\'est à but charicatif.', '<p style=\"text-align:center\"><strong>Un projet b&eacute;n&eacute;vole pour des b&eacute;n&eacute;voles.</strong></p>\r\n\r\n<p style=\"text-align:center\">Rien de plus &agrave; dire pour l&#39;instant.</p>\r\n\r\n<p style=\"text-align:center\">Le projet s&#39;articullera autour d&#39;une API php et d&#39;une application js .</p>\r\n\r\n<p style=\"text-align:center\">See you soon!</p>', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `project_skill`
--

CREATE TABLE `project_skill` (
  `project_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `project_skill`
--

INSERT INTO `project_skill` (`project_id`, `skill_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 16),
(1, 17),
(2, 1),
(2, 3),
(2, 4),
(2, 17),
(10, 1),
(10, 3),
(10, 4),
(10, 18),
(11, 1),
(11, 3),
(11, 4),
(11, 18),
(13, 2),
(13, 3),
(13, 4),
(13, 11),
(13, 17),
(13, 18),
(14, 1),
(14, 2),
(14, 4),
(14, 14),
(14, 16),
(14, 17),
(15, 2),
(15, 4),
(15, 11),
(15, 15),
(15, 16),
(15, 17),
(16, 2),
(16, 4),
(16, 11),
(16, 15);

-- --------------------------------------------------------

--
-- Structure de la table `skill`
--

CREATE TABLE `skill` (
  `id` int(11) NOT NULL,
  `icon_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `skill`
--

INSERT INTO `skill` (`id`, `icon_id`, `name`, `level`) VALUES
(1, 47, 'css', 4),
(2, 45, 'js', 2),
(3, 46, 'html', 3),
(4, 62, 'php', 3),
(5, 48, 'nodejs', 1),
(9, 49, 'sql', 2),
(11, 51, 'sass', 3),
(13, 55, 'vue', 1),
(14, 56, 'laravel', 1),
(15, 60, 'symfony', 2),
(16, 61, 'ajax', 2),
(17, 54, 'jquery', 3),
(18, 52, 'wordpress', 3),
(19, 59, 'react', 1),
(20, 53, 'python', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`) VALUES
(1, 'christopher.bonhomme@gmail.com', '[\"ROLE_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$Zcfd2wqbTZR+AkJmon2gmQ$KKk2nQsSYLIX8BI+R0ta0AjIJyhGrjCHmZQ9zVtaVwg');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `picture`
--
ALTER TABLE `picture`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_16DB4F89166D1F9C` (`project_id`);

--
-- Index pour la table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `project_skill`
--
ALTER TABLE `project_skill`
  ADD PRIMARY KEY (`project_id`,`skill_id`),
  ADD KEY `IDX_4D68EDE9166D1F9C` (`project_id`),
  ADD KEY `IDX_4D68EDE95585C142` (`skill_id`);

--
-- Index pour la table `skill`
--
ALTER TABLE `skill`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_5E3DE4775E237E06` (`name`),
  ADD UNIQUE KEY `UNIQ_5E3DE47754B9D732` (`icon_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `picture`
--
ALTER TABLE `picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT pour la table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `skill`
--
ALTER TABLE `skill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `picture`
--
ALTER TABLE `picture`
  ADD CONSTRAINT `FK_16DB4F89166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE SET NULL;

--
-- Contraintes pour la table `project_skill`
--
ALTER TABLE `project_skill`
  ADD CONSTRAINT `FK_4D68EDE9166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_4D68EDE95585C142` FOREIGN KEY (`skill_id`) REFERENCES `skill` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `skill`
--
ALTER TABLE `skill`
  ADD CONSTRAINT `FK_5E3DE47754B9D732` FOREIGN KEY (`icon_id`) REFERENCES `picture` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

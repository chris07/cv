<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class, [
                'required'   => true,
            ])
            ->add('prenom', TextType::class, [
                'required'   => true,
                'label'   => 'Prénom',  
            ])
            ->add('status', ChoiceType::class, [
                'choices'  => [
                    'Particulier' => 'Particulier',
                    'Entreprise' => 'Entreprise', 
                ],
            ])
            ->add('telephone', TextType::class, [
                'required'   => false,
                'label'   => 'Téléphone',  
            ])
            ->add('email', EmailType::class, [
                'required'   => true,
            ])
            ->add('content', TextareaType::class, [
                'required'   => true,
                'label'   => 'Message',  
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}

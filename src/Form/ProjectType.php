<?php

namespace App\Form;

use App\Entity\Project;
use App\Entity\Skill;
use App\Form\SkillsInputType;
use App\Repository\SkillRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityManagerInterface;

class ProjectType extends AbstractType
{

    private $slugger;
    private $skillRep;
    private $em;


    // Form types are services, so you can inject other services in them if needed
    public function __construct(SluggerInterface $slugger, SkillRepository $skillRep, EntityManagerInterface $em)
    {
        $this->slugger = $slugger;
        $this->skillRep = $skillRep;
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
            	'label' => 'Titre'])
            ->add('link', null, [
                'label' => 'Lien'])
            ->add('summary', TextareaType::class, [
                'label' => 'Résumé',
            ])
            ->add('content',CKEditorType::class, [
                'attr' => ['class' => 'ckeditor'],
            	'label' => 'Contenu',
                'jquery' => true
            ])
            ->add('skills', SkillsInputType::class, [
                'label' => 'Skills',
                'required' => false,
                'class' => Skill::class,
                'multiple' => true,
                'choice_label' => 'name',
                'by_reference' => false,
            ])
            ->add('images', FileType::class, [
                'label' => false,
                'multiple' => true,
                'mapped' => false,
                'required' => false,
            ])
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                /** @var Post */
                $project = $event->getData();
                if (null !== $projectTitle = $project->getTitle()) {
                    $project->setSlug($this->slugger->slug($projectTitle)->lower());
                }
            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
        ]);
    }
}

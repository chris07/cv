<?php

namespace App\Notification;

use App\Entity\Contact;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

/*
* 
*/
class ContactNotification
{
	/*
	* @var MailerInterface
	*/
	private $mailer;

	public function __construct(MailerInterface $mailer){

		$this->mailer = $mailer;
	}

	public function notify(Contact $contact){

		$email = (new TemplatedEmail())
		    ->from('contact@cv.lockmark.fr')
		    ->to('christopher.bonhomme@gmail.com')
		    ->addTo('contact@cv.lockmark.fr')
		    ->subject('Nouveau Message de Contact CV')
		    ->htmlTemplate('emails/contact.html.twig')
		    ->context([
		        'contact' => $contact,
		    ])
		;

		$this->mailer->send($email);
	}

}
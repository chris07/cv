<?php

namespace App\Repository;

use App\Entity\Project;
use App\Entity\ProjectSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function findAllVisible(ProjectSearch $search)
    {
    	$qb = $this->createQueryBuilder('p')
        ->select('p', 's', 'i', 'q')
    		->leftJoin('p.skills', 's')
            ->leftJoin('p.pictures', 'i')
            
    		->orderBy('p.id', 'DESC');

    	if(count($search->getSkills()) >0){

            $q=$search->getSkills();
           
            $k = 0;
            foreach ($q as $k => $skill) {
                $k++;
                $qb = $qb
                ->andWhere(":skill$k MEMBER OF p.skills")
                ->setParameter("skill$k", $skill);
            }
                
        }

    	return $query = $qb->leftJoin('s.icon', 'q')->getQuery();
    }

    public function findallWithSkills(){

        return $this->createQueryBuilder('p')
            ->select('p','s', 'i')
            ->leftJoin('p.skills', 's')
            ->leftJoin('s.icon', 'i')
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Project[] Returns an array of Project objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Project
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

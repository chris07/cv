<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class  ProjectSearch
{

	private $skills;

    public function __construct()
    {
        $this->skills = new ArrayCollection;
    }

	public function getSkills(): ArrayCollection
    {
        return $this->skills;
    }

    public function setSkills(ArrayCollection $skills): void
    {
        $this->skills = $skills;
    }
}
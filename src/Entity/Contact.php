<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * 
 */
class Contact{

	/**
	* @var string
	* @Assert\NotBlank()
	* @Assert\Length(min=2, max=50)
	* @Assert\Regex( 
	*	pattern="/[0-9]/",
	*	match=false,
	*	message="Le nom ne peut pas contenir de chiffres"
	*	)
	*/
	private $nom;

	/**
	* @var string
	* @Assert\NotBlank()
	* @Assert\Length(min=2, max=50)
	* @Assert\Regex( 
	*	pattern="/[0-9]/",
	*	match=false,
	*	message="Le prénom ne peut pas contenir de chiffres"
	*	)
	*/
	private $prenom;

	/**
	* @var string
	* @Assert\NotBlank()
	* @Assert\Choice({"Particulier", "Entreprise"})
	*/
	private $status;


	/**
	* @var string|null
	* @Assert\Regex( 
	*	pattern="/[0-9]{10}/",
	*	match=true,
	*	message="Entrez un numéro sous la forme 0506070809"
	*	)
	*/
	private $telephone;

	/**
	* @var string
	* @Assert\NotBlank()
	* @Assert\Email()
	*/
	private $email;


	/**
	* @var string
	* @Assert\NotBlank()
	* @Assert\Length(min=5, max=1500)
	*/
	private $content;

	public function getNom(): ?string
	{
	    return $this->nom;
	}

	public function SetNom(string $nom): self
	{
	    $this->nom = $nom;

	    return $this;
	}

	public function getPrenom(): ?string
	{
	    return $this->prenom;
	}

	public function setPrenom(string $prenom): self
	{
	    $this->prenom = $prenom;

	    return $this;
	}

	public function getStatus(): ?string
	{
	    return $this->status;
	}

	public function setStatus(string $status): self
	{
	    $this->status = $status;

	    return $this;
	}

	public function getTelephone(): ?string
	{
	    return $this->telephone;
	}

	public function setTelephone(string $telephone): self
	{
	    $this->telephone = $telephone;

	    return $this;
	}

	public function getEmail(): ?string
	{
	    return $this->email;
	}

	public function setEmail(string $email): self
	{
	    $this->email = $email;

	    return $this;
	}

	public function getContent(): ?string
	{
	    return $this->content;
	}

	public function setContent(string $content): self
	{
	    $this->content = $content;

	    return $this;
	}
	
}
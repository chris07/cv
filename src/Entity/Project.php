<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use App\Entity\Skill;
use App\Entity\Picture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 * @UniqueEntity(
 *     fields={"title", "link", "slug"},
 *     errorPath="title",
 *     message="Ce titre existe déjà."
 * )
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     @Assert\Length(
     *      min = 5,
     *      max = 50,
     *      minMessage = "Le titre doit contenir au moins {{ limit }} caractères",
     *      maxMessage = "Le titre ne peut dépasser {{ limit }} caractères",
     *      allowEmptyString = false
     * )
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="post.blank_summary")
     * @Assert\Length(max=255)
     */
    private $summary;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(
     *      min = 5,
     *      max = 5000,
     *      minMessage = "Le contenu doit contenir au moins {{ limit }} caractères",
     *      maxMessage = "Le contenu ne peut dépasser {{ limit }} caractères",
     *      allowEmptyString = false
     * )
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     * @Assert\Url(
     *    message = "Le lien '{{ value }}' n'est pas une url valide.",
     *)
     */
    private $link;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Skill", cascade={"persist"})
     * @ORM\JoinTable(name="project_skill")
     * @ORM\OrderBy({"name": "ASC"})
     * @Assert\Count(max="10", maxMessage="Trop de skills")
     *
     */
    private $skills;

    /**
     * @ORM\OneToMany(targetEntity=Picture::class, mappedBy="project", cascade={"persist"})
     */
    private $pictures;

    public function __construct()
    {
        $this->skills = new ArrayCollection();
        $this->pictures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(string $summary): void
    {
        $this->summary = $summary;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function addSkill(Skill ...$skills): void
    {
        foreach ($skills as $skill) {
            if (!$this->skills->contains($skill)) {
                $this->skills->add($skill);
            }
        }
    }

    public function removeSkill(Skill $skill): void
    {
        $this->skills->removeElement($skill);
    }

    public function getSkills(): Collection
    {
        return $this->skills;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures[] = $picture;
            $picture->setProject($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->pictures->contains($picture)) {
            $this->pictures->removeElement($picture);
            // set the owning side to null (unless already changed)
            if ($picture->getProject() === $this) {
                $picture->setProject(null);
            }
        }

        return $this;
    }
}

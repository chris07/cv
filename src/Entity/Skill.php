<?php

namespace App\Entity;

use App\Repository\SkillRepository;
use App\Entity\Project;
use App\Entity\Picture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=SkillRepository::class)
 * @UniqueEntity(
 *     fields={"name"},
 *     errorPath="name",
 *     message="Ce nom existe déjà."
 * )
 */
class Skill implements \JsonSerializable
{

    const LEVEL = [
        0 => "Null",
        1 => "Débutant",
        2 => "Intermédiaire",
        3 => "Confirmé",
        4 => "Expert"
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity=picture::class, cascade={"persist", "remove"})
     */
    private $icon;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $level;

    public function __construct(){
        $this->level = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        
        return $this;
    }

    public function getIcon(): ?picture
    {
        return $this->icon;
    }

    public function setIcon(?picture $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize(): string
    {
        // This entity implements JsonSerializable (http://php.net/manual/en/class.jsonserializable.php)
        // so this method is used to customize its JSON representation when json_encode()
        // is called, for example in tags|json_encode (templates/form/fields.html.twig)

        return $this->name;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getFormatedLevel(): string
    {
        return self::LEVEL[$this->level];
    }

    
}

<?php

namespace App\Controller;

use App\Entity\Project;
use App\Repository\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;

class HomeController extends AbstractController
{
    private $repository;
	private $em;

	public function __construct(ProjectRepository $repository , EntityManagerInterface $em)
	{
		$this->repository = $repository;
		$this->em = $em;
	}

    /**
     * @Route("/", name="accueil")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'active_menu' => 'home'
        ]);
    }
}

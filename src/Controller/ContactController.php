<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Notification\ContactNotification;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ContactType;
use Symfony\Component\Mailer\MailerInterface;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index(Request $request, ContactNotification $notification)
    {

        $contact = new Contact();
    	$form = $this->createForm(ContactType::class, $contact);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			
			//Envoi du mail
            $notification->notify($contact);
            
			$this->addFlash('success', 'Message envoyé avec succès');

			return $this->redirectToRoute('contact');
		}

        return $this->render('contact/index.html.twig', [
            'active_menu' => 'contact',
            'form' => $form->createView(),
        ]);
    }
}

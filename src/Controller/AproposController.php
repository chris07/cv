<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\SkillRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class AproposController extends AbstractController
{

    private $repository;

    public function __construct(SkillRepository $repository){

        $this->repository = $repository;
    }

    /**
     * @Route("/apropos", name="apropos")
     */
    public function index()
    {

        $skills = $this->repository->findAllWithPictures();

        return $this->render('apropos/index.html.twig', [
            'skills' => $skills, 
        ]);
    }

    /**
     * @Route("/cv", name="cv")
     */
    public function show()
    {

        $skills =$this->repository->findAllWithPictures();

        return $this->render('apropos/show.html.twig', [
            'skills' => $skills, 
        ]);
    }
}

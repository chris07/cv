<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\ProjectSearch;
use App\Form\ProjectSearchType;
use App\Repository\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;


class ProjectController extends AbstractController
{
    private $repository;
	private $em;

	public function __construct(ProjectRepository $repository , EntityManagerInterface $em)
	{
		$this->repository = $repository;
		$this->em = $em;
	}

    /**
     * @Route("/projects", name="projects")
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {
        $search = new ProjectSearch;
        $form = $this->createForm(ProjectSearchType::class, $search);
        $form->handleRequest($request);

    	$pagination = $paginator->paginate(
            $this->repository->findAllVisible($search), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            3 /*limit per page*/
        );

        return $this->render('project/index.html.twig', [
            'active_menu' => 'projects',
            'pagination' => $pagination,
            'form' =>$form->createView(),
        ]);
    }

    /**
     * @Route("/projet/{slug}-{id}", name="project.show", requirements={"slug": "[a-z0-9\-]*"})
     */
    public function show(Project $project, $slug){

        if ($project->getSlug() !== $slug){
            return $this->redirectToRoute('project.show', [
                'id' => $project->getID(),
                'slug' => $project->getSlug()
            ], 301);
        }

        return $this->render('project/project.html.twig', [
        	'active_menu' => 'not',
            'project' => $project
        ]);
    }
}

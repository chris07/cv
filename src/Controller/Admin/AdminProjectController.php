<?php

namespace App\Controller\Admin;

use App\Entity\Project;
use App\Entity\Skill;
use App\Entity\Picture;
use App\Repository\ProjectRepository;
use App\Repository\SkillRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\ProjectType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Helpers\ImagesHelper;



class AdminProjectController extends AbstractController
{

	private $repository;
	private $em;

	public function __construct(ProjectRepository $repository, EntityManagerInterface $em){

		$this->repository = $repository;
		$this->em = $em;

	}

	/**
     * @Route("/admin/projects", name="admin.projects.index", methods={"GET"})
     */
	public function index(){

		$projects =$this->repository->findallWithSkills();

		return $this->render('admin/projects/index.html.twig', [
			'projects' => $projects, 
			'active_menu' => 'projects_admin',
		]);
	}

	/**
    * @Route("/admin/project/create", name="admin.project.create", methods={"GET","POST"})
    */
	public function new(Request $request, ImagesHelper $helper) :Response{

		$project = new Project;
		$form = $this->createForm(ProjectType::class, $project);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {

			$images = $form->get('images')->getData();

			if($images){

				if($helper->savePics($images, $project)){

					$this->em->persist($project);
					$this->em->flush();

					$this->addFlash('success', 'Projet crée avec succès');

					return $this->redirectToRoute('admin.projects.index');
				}
				else{

					$error = "mauvais format d'image";
					$this->addFlash('error', $error);

					return $this->render('admin/projects/create.html.twig', [		
						'form' => $form->createView(),
					]);
				}
			}
			else{

				$this->em->persist($project);
				$this->em->flush();

				$this->addFlash('success', 'Projet crée avec succès');

				return $this->redirectToRoute('admin.projects.index');
			}	
		}

		return $this->render('admin/projects/create.html.twig', [		
			'form' => $form->createView(),
		]);
	}

	/**
     * @Route("/admin/project/{id}", name="admin.project.edit", methods={"GET","POST"})
     */
	public function edit( Project $project, Request $request, ImagesHelper $helper){

		$form = $this->createForm(ProjectType::class, $project);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {

			$images = $form->get('images')->getData();

			if($images){

				if($helper->savePics($images, $project)){

					$this->getDoctrine()->getManager()->flush();

				    $this->addFlash('success', 'Projet modifiée avec succès');

					return $this->redirectToRoute('admin.project.edit', ['id' => $project->getId()]);
				}
				else{

					$error = "mauvais format d'image";
					$this->addFlash('error', $error);

					return $this->render('admin/projects/edit.html.twig', [
						'project' => $project, 
						'form' => $form->createView(),
					]);
				}   
			}
			else{

				$this->getDoctrine()->getManager()->flush();

				$this->addFlash('success', 'Projet modifiée avec succès');

				return $this->redirectToRoute('admin.project.edit', ['id' => $project->getId()]);
			}
		}

		return $this->render('admin/projects/edit.html.twig', [
			'project' => $project, 
			'form' => $form->createView(),
		]);
	}


	/**
	* @Route("/admin/project/{id}/delete", name="admin.project.delete", methods={"POST"})
	*/
	public function delete( Project $project, Request $request){

		$submittedToken = $request->request->get('token');

		if ($this->isCsrfTokenValid('delete-item', $submittedToken)) {

			$imgs = $project->getPictures();
			foreach ($imgs as $img) {
				$image = $img->getName();
				unlink($this->getParameter('images_directory').'/'.$image);
			}

		    $this->em->remove($project);
			$this->em->flush();

			$this->addFlash('success', 'Projet supprimmée avec succès');
		}

		return $this->redirectToRoute('admin.projects.index');
	}

	/**
	* @Route("/admin/project/delete/img/{id}", name="admin.project.delete.img", methods={"DELETE"})
	*/
	public function deletePicture(Picture $img, Request $request){

		$data = json_decode($request->getContent(), true);

		if ($this->isCsrfTokenValid('delete'. $img->getId(), $data['_token'])) {

			$nom = $img->getName();
			unlink($this->getParameter('images_directory').'/'.$nom);

			$this->em->remove($img);
			$this->em->flush();

			return new JsonResponse(['success' => 1]);
		}
		else{

			return new JsonResponse(['error' => 'Token Invalide!'], 400);
		}
	}
}
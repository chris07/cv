<?php
namespace App\Controller\Admin;

use App\Entity\Project;
use App\Entity\Skill;
use App\Entity\Picture;
use App\Repository\ProjectRepository;
use App\Repository\SkillRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\SkillType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Helpers\ImagesHelper;



class AdminSkillController extends AbstractController
{

	private $repository;
	private $em;

	public function __construct(SkillRepository $repository, EntityManagerInterface $em){

		$this->repository = $repository;
		$this->em = $em;

	}

	/**
     * @Route("/admin/skills", name="admin.skills.index", methods={"GET"})
     */
	public function index(){

		$skills =$this->repository->findAllWithPictures();

		return $this->render('admin/skills/index.html.twig', [
			'active_menu' => 'skills_admin',
			'skills' => $skills, 
		]);
	}

	/**
    * @Route("/admin/skill/create", name="admin.skill.create", methods={"GET","POST"})
    */
	public function new(Request $request, ImagesHelper $helper) :Response{

		$skill = new Skill;
		$form = $this->createForm(SkillType::class, $skill);
		$form->handleRequest($request);
		

		if ($form->isSubmitted() && $form->isValid()) {

			$image = $form->get('icon')->getData();

			if($image){

				if($helper->savePic($image, $skill)){

					$this->em->persist($skill);
					$this->em->flush();

					$this->addFlash('success', 'Skill créé avec succès');

					return $this->redirectToRoute('admin.skills.index');
				}
				else{

					$error = "mauvais format d'image";
					$this->addFlash('error', $error);

					return $this->render('admin/skills/create.html.twig', [		
						'form' => $form->createView(),
					]);
				}
			}
			else{

				$this->em->persist($skill);
				$this->em->flush();

				$this->addFlash('success', 'Skill créé avec succès');

				return $this->redirectToRoute('admin.skills.index');
			}

			
		}

		return $this->render('admin/skills/create.html.twig', [		
			'form' => $form->createView(),
		]);
	}

	/**
     * @Route("/admin/skill/{id}", name="admin.skill.edit", methods={"GET","POST"})
     */
	public function edit(Skill $skill, Request $request, EntityManagerInterface $em, ImagesHelper $helper){


		$form = $this->createForm(SkillType::class, $skill);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {

			$image = $form->get('icon')->getData();

			if($image){

				if($helper->savePic($image, $skill)){

					$this->getDoctrine()->getManager()->flush();

					$this->addFlash('success', 'Skill modifiée avec succès');

					return $this->redirectToRoute('admin.skill.edit', ['id' => $skill->getId()]);
				}
				else{

					$error = "mauvais format d'image";
					$this->addFlash('error', $error);

					return $this->render('admin/skills/edit.html.twig', [
						'skill' => $skill, 
						'form' => $form->createView(),
					]);
				}
			}
			else{

				$this->getDoctrine()->getManager()->flush();

				$this->addFlash('success', 'Skill modifiée avec succès');

				return $this->redirectToRoute('admin.skill.edit', ['id' => $skill->getId()]);
			}


		}

		return $this->render('admin/skills/edit.html.twig', [
			'skill' => $skill, 
			'form' => $form->createView(),
		]);
	}

	/**
	* @Route("/admin/skill/create/ajax/{name}", name="admin.skill.ajax.create", methods={"POST"})
	*/
	public function newAjax($name) :Response
	{

		$skill = new Skill;
		$skill->setName(trim(strip_tags($name)));
		$this->em->persist($skill);
		$this->em->flush();
		$id = $skill->getId();

		return new JsonResponse(['id' => $id]);
	}


	/**
	* @Route("/admin/skill/{id}/delete", name="admin.skill.delete", methods={"POST"})
	*/
	public function delete(Skill $skill, Request $request){

		$submittedToken = $request->request->get('token');

		if ($this->isCsrfTokenValid('delete-item', $submittedToken)) {

			if($skill->getIcon() !== null){
				$img = $skill->getIcon()->getName();
				unlink($this->getParameter('images_directory').'/'.$img);
			}
			
		    $this->em->remove($skill);
			$this->em->flush();

			$this->addFlash('success', 'Skill supprimmée avec succès');
		}

		return $this->redirectToRoute('admin.skills.index');
	}
}
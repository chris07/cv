<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class PolicyController extends AbstractController
{

    /**
     * @Route("/policy", name="policy")
     */
    public function index()
    {
        return $this->render('policy/index.html.twig');
    }
}

<?php

namespace App\Helpers;

use App\Entity\Project;
use App\Entity\Skill;
use App\Entity\Picture;
use App\Form\ProjectType;
use App\Form\SkillType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ImagesHelper extends AbstractController
{
	/**
	*	enregistrement de l'image du skill
	**/
	public function savePic($image, Skill $skill){

		$ext = $image->guessExtension();
		$extOK = ['png','jpg','webp','jpeg','svg'];
		$fichierOK = in_array($ext, $extOK) ? true : false;

		if($image !== null && $fichierOK && $this->verifyPic($image)){
			
			$fichier = md5(uniqid()). '.' . $image->guessExtension();
			$image->move( $this->getParameter('images_directory'), $fichier );
			$img = new Picture();
			$img->setName($fichier);
			$skill->setIcon($img);

			return true;
		}
		else{
			return false;
		}
	}

	/**
	*	Fonction d'enregistrement des images du projet
	**/
	public function savePics($images, Project $project){

		$return = false;

		foreach ($images as $image){

			$ext = $image->guessExtension();
			$extOK = ['png','jpg','webp','jpeg','svg'];
			$fichierOK = in_array($ext, $extOK) ? true : false;

			if($image !== null && $fichierOK && $this->verifyPic($image)){

				$fichier = md5(uniqid()). '.' . $image->guessExtension();
				$image->move( $this->getParameter('images_directory'), $fichier );
				$img = new Picture();
				$img->setName($fichier);
				$project->addPicture($img);
				$return = true;
			}
			else{
				return false;
			}	
		}
		return $return;
	}

	/**
	*	Fonction de Vérification des images
	**/
	private function verifyPic($pic){

		$return = false;
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mime = finfo_file($finfo, $pic);
		$name = $pic->getClientOriginalName();
		$nameOK = strpos('<', $name);
		
		if($pic->getsize() && strpos($mime, 'image/') === 0 && !$nameOK){
			$return = true;
		}

		finfo_close($finfo);

		return $return;
	}
}